FROM python:3

# do not buffer input and output, send out to env
ENV PYTHONUNBUFFERED 1
RUN mkdir /website
WORKDIR /website

# copy from local folder (current dir) and put in the container
COPY . /website/

# Installs our project dependencies
RUN pip install -r requirements.txt
