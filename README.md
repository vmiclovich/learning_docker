# Installation

## Windows

Visit [https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/)

## Mac OS X

Visit [https://docs.docker.com/docker-for-mac/install/](https://docs.docker.com/docker-for-mac/install/)

## Linux

Visit [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

###  Install Docker compose
[https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

### Build your Docker image

`docker-compose build`

### Run your Docker image

`docker-compose up`

### Next steps

Try to migrate your database in a separate window/terminal

`docker-compose run web python src/shop/manage.py migrate`

You can confirm by visiting the MySQL client

`docker-compose run --rm client`